const jwt = require('jsonwebtoken');
const authConfig = require('../app/config/auth');
const { promisify } = require('util');

class JWT {
    async generateToken(id){
        return jwt.sign({id}, authConfig.secret, {
            expiresIn: authConfig.expiresIn
        });
    }

    async verifyToken(token) {
        try{
            return await promisify(jwt.verify)(token, authConfig.secret);
        }catch(err){
            return err;
        }
    }
}

module.exports = new JWT();