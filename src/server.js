const app = require('./app');
require('dotenv/config');

app.listen(3030, () => {
    console.log('Server is running in port 3030.');
});