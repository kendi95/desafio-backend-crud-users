const routes = require('express').Router();

const UserMiddleware = require('./app/middlewares/user.middleware');
const AuthMiddleware = require('./app/middlewares/auth.middleware');
const SessionMiddleware = require('./app/middlewares/session.middleware');
const ClinicaMiddleware = require('./app/middlewares/clinica.middleware');

const UserController = require('./app/controller/UserController');
const SessionController = require('./app/controller/SessionController');
const ClinicaController = require('./app/controller/ClinicaController');

routes.post('/users', UserMiddleware.store, UserController.store);
routes.post('/sessions', SessionMiddleware, SessionController.store);
routes.post('/clinicas', ClinicaMiddleware, ClinicaController.store);

routes.use(AuthMiddleware);

routes.put('/users/:id', UserMiddleware.update, UserController.update);
routes.get('/users/:id', UserMiddleware.show, UserController.show);

routes.get('/clinicas', ClinicaController.index);
routes.get('/clinicas/:name', ClinicaController.show);


module.exports = routes;