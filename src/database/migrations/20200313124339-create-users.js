'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.createTable('users', { 
        id: {
          type: Sequelize.INTEGER,
          allowNull: false,
          autoIncrement: true,
          primaryKey: true
        } ,
        name: {
          type: Sequelize.STRING,
          allowNull: false
        },
        email: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true
        },
        password: {
          type: Sequelize.STRING,
          allowNull: false
        },
        telephone: {
          type: Sequelize.STRING,
          allowNull: false
        },
        level: {
          type: Sequelize.STRING,
          allowNull: false,
        },
        crm: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        situation: {
          type: Sequelize.STRING,
          allowNull: true
        },
        subscribe_type: {
          type: Sequelize.STRING,
          allowNull: true
        },
        speciality_id: {
          type: Sequelize.INTEGER,
          allowNull: true
        },
        address_id: {
          type: Sequelize.INTEGER,
          allowNull: false
        },
        created_at: {
          type: Sequelize.DATE,
          allowNull: false
        },
        updated_at: {
          type: Sequelize.DATE,
          allowNull: false
        }
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('users');
  }
};
