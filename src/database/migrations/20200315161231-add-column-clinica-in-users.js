'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
      return queryInterface.addColumn('users', 'clinica_id', {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: { model: 'clinicas', key: 'id' }
      });
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.removeColumn('users', 'clinica_id');
  }
};
