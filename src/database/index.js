const Sequelize = require('sequelize');
const dbConfig = require('../app/config/database');

const User = require('../app/models/User');
const Address = require('../app/models/Address');
const Speciality = require('../app/models/Speciality');
const Clinica = require('../app/models/Clinica');

const models = [User, Address, Speciality, Clinica];

class Database {

    constructor() {
        this.init();
    }

    init() {
        this.connection = new Sequelize(dbConfig);
        models.map(model => model.init(this.connection))
            .map(model => model.associate && model.associate(this.connection.models));
    }

}

module.exports = new Database();