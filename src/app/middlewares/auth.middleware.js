const JWT = require('../../utils/jwt');

module.exports = async (req, res, next) => {
    const { authorization }  = req.headers;

    if(! authorization){
        return res.status(400).json({ error: 'Token is not provided.' });
    }

    const [_, token] = authorization.split(' ');

    const isVerified = await JWT.verifyToken(token);

    if(! isVerified || isVerified.name === 'TokenExpiredError') {
        return res.status(401).json({ error: 'Token is expired.' });
    }

    const { id } = isVerified;
    req.userId = id;

    next();
}