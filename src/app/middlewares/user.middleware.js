const Yup = require('yup');
const User = require('../models/User');
const Level = require('../models/enums/Level');

module.exports = {
    async store(req, res, next){
        const schema = Yup.object().shape({
            name: Yup.string().required(),
            email: Yup.string().email().required(),
            password: Yup.string().min(6).required(),
            telephone: Yup.string().required(),
            level: Yup.string().required(),
            clinica_id: Yup.number().required()
        });
        const schemaMedic = Yup.object().shape({
            crm: Yup.number().required(),
            speciality: Yup.string().required(),
            uf: Yup.string().required(),
            cidade: Yup.string().required(),
            situation: Yup.string().required(),
            subscribe_type: Yup.string().required()
        });
        const schemaPacient = Yup.object().shape({
            logradouro: Yup.string().required(),
            bairro: Yup.string().required(),
            cep: Yup.string().required(),
            uf: Yup.string().required(),
            cidade: Yup.string().required(),
        });
    
        if(! (await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation error or fields is required.' });
        }
    
        const ifExists = await User.findOne({
            where: {email: req.body.email}
        });
        if(ifExists){
            return res.status(400).json({ error: 'Exists user with same email.' });
        }
    
        if(req.body.level === Level.medico){ 
            if( ! (await schemaMedic.isValid(req.body))){
                return res.status(400).json({ error: 'Validation error or fields is required.' })
            }
            next();
        } else {
            if( ! (await schemaPacient.isValid(req.body))){
                return res.status(400).json({ error: 'Validation error or fields is required.' })
            }
            next();
        }
    
        
    },

    async update(req, res, next) {
        const schema = Yup.object().shape({
            name: Yup.string().required(),
            email: Yup.string().email().required(),
            telephone: Yup.string().required(),
            level: Yup.string().required(),
            clinica_id: Yup.number().required()
        });
        const schemaMedic = Yup.object().shape({
            crm: Yup.number().required(),
            speciality: Yup.string().required(),
            uf: Yup.string().required(),
            cidade: Yup.string().required(),
            situation: Yup.string().required(),
            subscribe_type: Yup.string().required()
        });
        const schemaPacient = Yup.object().shape({
            logradouro: Yup.string().required(),
            bairro: Yup.string().required(),
            cep: Yup.string().required(),
            uf: Yup.string().required(),
            cidade: Yup.string().required(),
        });

        if(! (await schema.isValid(req.body))){
            return res.status(400).json({ error: 'Validation error or fields is required.' });
        }

        if(req.body.level === Level.medico){ 
            if( ! (await schemaMedic.isValid(req.body))){
                return res.status(400).json({ error: 'Validation error or fields is required.' })
            }
            next();
        } else {
            if( ! (await schemaPacient.isValid(req.body))){
                return res.status(400).json({ error: 'Validation error or fields is required.' })
            }
            next();
        }
    },

    async show(req, res, next) {
        const { id } = req.params;
        if(req.userId !== Number(id)) {
            return res.status(403).json({ error: 'You have not permission to read this data.' });
        }

        next();
    }
    

}