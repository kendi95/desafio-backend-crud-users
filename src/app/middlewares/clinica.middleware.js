const Yup = require('yup');

module.exports = async (req, res, next) => {
    const schema = Yup.object().shape({
        name: Yup.string().required()
    });

    if(! (await schema.isValid(req.body))){
        return res.status(400).json({ error: 'Validation error or fields is required.' });
    }

    next();
}