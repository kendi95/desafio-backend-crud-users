const User = require('../models/User');
const Yup = require('yup');

module.exports = async (req, res, next) => {
    const schema = Yup.object().shape({
        email: Yup.string().email().required(),
        password: Yup.string().required()
    });
    if(! (await schema.isValid(req.body))){
        return res.status(400).json({ error: 'Validation error or fields is required.' });
    }

    const { email, password } = req.body;

    const user = await User.findOne({
        where: {email}
    });
    if(!user){
        return res.status(401).json({ error: 'User does not exists.' });
    }

    if(!(await user.checkPassword(password))){
        return res.status(401).json({ error: 'Password does not match.' });
    }

    req.newUser = {
        id: user.id,
        level: user.level,
        name: user.name
    }

    next();
}