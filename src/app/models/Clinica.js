const { Model, Sequelize } = require('sequelize');

class Clinica extends Model {
    static init(sequelize){
        super.init({
            name: Sequelize.STRING,
        },{
            sequelize
        });

        return this;
    }

    static associate(models){
        this.hasMany(models.User);
    }

}

module.exports = Clinica;