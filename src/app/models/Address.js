const { Model, Sequelize } = require('sequelize');

class Address extends Model {
    static init(sequelize){
        super.init({
            logradouro: Sequelize.STRING,
            bairro: Sequelize.STRING,
            cep: Sequelize.STRING,
            cidade: Sequelize.STRING,
            uf: Sequelize.STRING
        },{
            sequelize
        });

        return this;
    }

}

module.exports = Address;