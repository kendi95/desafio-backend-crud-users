const { Model, Sequelize } = require('sequelize');
const bcrypt = require('bcryptjs');

class User extends Model {
    static init(sequelize){
        super.init({
            name: Sequelize.STRING,
            email: Sequelize.STRING,
            password: Sequelize.STRING,
            telephone: Sequelize.STRING,
            level: Sequelize.STRING,
            crm: Sequelize.INTEGER,
            situation: Sequelize.STRING,
            subscribe_type: Sequelize.STRING
        },{
            sequelize
        });

        this.addHook('beforeCreate', async (user) => {
            user.password = await bcrypt.hash(user.password, 10);
            return this;
        });

        this.addHook('beforeUpdate', async (user) => {
            user.password = await bcrypt.hash(user.password, 10);
            return this;
        })

        return this;
    }

    static associate(models){
        this.belongsTo(models.Speciality, { foreignKey: 'speciality_id', as: 'speciality' });
        this.belongsTo(models.Address, { foreignKey: 'address_id', as: 'address' });
        this.belongsTo(models.Clinica, { foreignKey: 'clinica_id', as: 'clinica' });
    }

    checkPassword(password){
        return bcrypt.compare(password, this.password);
    }
}

module.exports = User;