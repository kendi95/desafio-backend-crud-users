const { Model, Sequelize } = require('sequelize');

class Speciality extends Model {
    static init(sequelize){
        super.init({
            name: Sequelize.STRING,
        },{
            sequelize
        });

        return this;
    }

}

module.exports = Speciality;