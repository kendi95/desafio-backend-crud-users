const SessionService = require('../services/SessionService');

class SessionController {
    async store(req, res) {
        const token = await SessionService.generateToken(req);
        return res.json(token);
    }
}

module.exports = new SessionController();