const UserService = require('../services/UserService');

class UserController {
    async store(req, res) {
        const user = await UserService.createUser(req);
        if(user.error){
            return res.status(400).json(user);
        }
        return res.status(201).json(user);
    }

    async update(req, res) {
        const user = await UserService.updateUser(req);
        if(user.error){
            return res.status(400).json(user);
        }
        return res.json(user);
    }

    async show(req, res) {
        const user = await UserService.getUser(req);
        if(user.error){
            return res.status(400).json(user);
        }
        return res.json(user);
    }
}

module.exports = new UserController();