const ClinicaService = require('../services/ClinicaService');

class ClinicaController {
    async store(req, res) {
        const clinica = await ClinicaService.store(req);
        if(clinica.error){
            return res.status(400).json(clinica);
        }
        return res.status(201).json(clinica);
    }

    async index(req, res) {
        const clinicas = await ClinicaService.index(req);
        return res.status(200).json(clinicas);
    }

    async show(req ,res) {
        const clinica = await ClinicaService.show(req);
        if(clinica.error){
            return res.status(400).json(clinica);
        }
        return res.status(200).json(clinica);
    }
}

module.exports = new ClinicaController();