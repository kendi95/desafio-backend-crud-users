const Level = require('../models/enums/Level');

const Speciality = require('../models/Speciality');
const Address = require('../models/Address');
const User = require('../models/User');
const Clinica = require('../models/Clinica');

class UserService {
    async createUser(req){
        const { name, email, password, level, telephone, clinica_id } = req.body;

        const clinica = await Clinica.findByPk(clinica_id, { attributes: ['id'] });
        if(!clinica){
            return { error: 'Clinica does not exists.' }
        }

        if(req.body.level === Level.medico){
            const { crm, speciality, uf, cidade, situation, subscribe_type } = req.body;

            const special = await Speciality.create({name: speciality});
            const address = await Address.create({ uf, cidade });
            const user = await User.create({
                name,
                email,
                password,
                telephone,
                level,
                crm,
                speciality_id: special.id,
                address_id: address.id,
                situation,
                subscribe_type,
                clinica_id: clinica.id
            });

            return user;
        } else {
            const { logradouro, cep, bairro, uf, cidade } = req.body;

            const address = await Address.create({
                logradouro,
                bairro,
                cep,
                uf,
                cidade
            });
            const user = await User.create({
                name,
                email,
                password,
                telephone,
                level,
                address_id: address.id,
                clinica_id: clinica.id
            })

            return user;
        }
    }

    async updateUser(req){
        const { name, email, level, telephone, clinica_id } = req.body;

        const clinica = await Clinica.findByPk(clinica_id, { attributes: ['id'] });
        if(!clinica){
            return { error: 'Clinica does not exists.' }
        }

        if(req.body.level === Level.medico){
            const { crm, speciality, uf, cidade, situation, subscribe_type } = req.body;

            let user = await User.findByPk(req.params.id);
            if(!user){
                return { error: 'User does not exists.' };
            }

            await Speciality.update({
                name: speciality
            }, {
                where: {
                    id: user.speciality_id
                }
            });

            await Address.update({
                uf, cidade
            }, {
                where: {
                    id: user.address_id
                }
            });

            user.name = name;
            user.email = email;
            user.telephone = telephone;
            user.crm = crm;
            user.situation = situation;
            user.subscribe_type = subscribe_type;

            await user.save();
            return user;
        } else {
            const { logradouro, cep, bairro, uf, cidade } = req.body;

            let user = await User.findByPk(req.params.id);
            if(!user){
                return { error: 'User does not exists.' };
            }

            await Address.update({
                uf, cidade, logradouro, cep, bairro
            }, {
                where: {
                    id: user.address_id
                }
            });

            user.name = name;
            user.email = email;
            user.telephone = telephone;

            await user.save();
            return user;
        }
    }

    async getUser(req){
        const { id } = req.params;
        const user = await User.findByPk(Number(id), {
            attributes: ['id', 'name', 'email', 'telephone', 'level', 'crm', 'situation', 'subscribe_type'],
            include: [
                {
                    model: Speciality,
                    as: 'speciality',
                    attributes: ['id', 'name']
                },
                {
                    model: Address,
                    as: 'address',
                    attributes: ['id', 'uf', 'cidade', 'logradouro', 'cep', 'bairro']
                }
            ]
        });
        if(!user){
            return { error: 'User does not exists.' };
        }

        if(user.level === Level.medico){
            return {
                id: user.id,
                name: user.name,
                telephone: user.telephone,
                level: user.level,
                crm: user.crm,
                situation: user.situation,
                subscribe_type: user.subscribe_type,
                speciality: user.speciality,
                address: {
                    id: user.address.id,
                    uf: user.address.uf,
                    cidade: user.address.cidade
                }
            }
        } else {
            return {
                id: user.id,
                name: user.name,
                telephone: user.telephone,
                level: user.level,
                address: user.address
            }
        }

    }
}

module.exports = new UserService();