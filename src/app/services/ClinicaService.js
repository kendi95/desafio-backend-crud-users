const Clinica = require('../models/Clinica');
const User = require('../models/User');
const Speciality = require('../models/Speciality');
const Address = require('../models/Address');

class ClinicaService {
    async store(req){
        const { name } = req.body;
        let clinica = await Clinica.findOne({
            where: { name }
        });
        if(clinica) {
            return { error: 'You can not create same clinica.' };
        }
    
        clinica = await Clinica.create({ name });
        return clinica;
    }

    async index(req) {
        const clinicas = await Clinica.findAll({ attributes: ['id', 'name'] });
        return clinicas;
    }

    async show(req) {
        const { name } = req.params;
        const clinica = await Clinica.findOne({
            where: { name },
            attributes: ['id', 'name']
        });
        if(!clinica) {
            return { error: 'Clinica does not exists.' }
        }

        const users = await User.findAll({
            where: { clinica_id: clinica.id },
            attributes: [
                'id', 
                'name', 
                'email', 
                'telephone', 
                'level', 
                'crm', 
                'situation',
                'subscribe_type'],
            include: [
                {
                    model: Speciality,
                    as: 'speciality',
                    attributes: ['id', 'name']
                },
                {
                    model: Address,
                    as: 'address',
                    attributes: ['id', 'logradouro', 'bairro', 'cep', 'uf', 'cidade']
                }
            ]
        });

        const newClinica = {
            id: clinica.id,
            name: clinica.name,
            users
        }

        return newClinica;
    }
}

module.exports = new ClinicaService();