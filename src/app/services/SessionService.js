const JWT = require('../../utils/jwt');

class SessionService {
    async generateToken(req){
        const { id, level, name } = req.newUser;
        const token = await JWT.generateToken(id);
        return {
            name,
            level,
            token
        }
    }
}

module.exports = new SessionService();